%Entrenamiento de un sistema difuso para la predicción de un prestamo bancario 
%con entradas edad, salario y capacidad de pago empleando algoritmo
%genetico

close all
clear all
warning off

%Opciones
options = optimset('Display','iter','MaxIter',18);

%Valores iniciales
 difuso = [21 74 18 21 18 60 18 74 248 800 248 400 248 600 248 800 5 300 5 75 5 150 5 200 0 45 2 12 20 17 30 45 0 25 0 3 6 5 11 18 16 19 22]

%% Simulacion para plazo

%Función de minimización fminunc para plazo
[difuso,fval,exitflag,output] = fminunc(@JobjetivoPl,difuso,options);

%Sistema difuso optimizado
FIS = sisdifuso(difuso);

%Datos para comprobación
X = [ecgEdad...
    ecgSalario...
    ecgCapacidad]

Y = [xlsread('datosBanco.xlsx','Analisis','D3:D53')]

%Evaluación del sistema difuso
yd = evalfis(X,FIS);
yd = yd(:,1)

%Error obtenido
error=Y-yd;
msePlazo=1/length(error)*sum(error.^2)

%Presentación de los resultados
figure
title('Comparación')
plot(yd)
hold on
plot(Y)
legend('plazo simulado','plazo real')
xlabel('X');ylabel('Datos')

%Figura del error
figure
plot(error)
title('Error')
xlabel('X');ylabel('Error')

%% Simulacion para prestamo

%Función de minimización fminunc para prestamo
[difuso,fval,exitflag,output] = fminunc(@JobjetivoPre,difuso,options);

%Sistema difuso optimizado
FIS = sisdifuso(difuso);

%Datos para comprobación
X = [ecgEdad...
    ecgSalario...
    ecgCapacidad]

Y = [xlsread('datosBanco.xlsx','Analisis','E3:E53')]

%Evaluación del sistema difuso
yd = evalfis(X,FIS);
yd = yd(:,2)

%Error obtenido
error=Y-yd;
msePrestamo=1/length(error)*sum(error.^2)

%Presentación de los resultados
figure
title('Comparación')
plot(yd)
hold on
plot(Y)
legend('prestamo simulado','prestamo real')
xlabel('X');ylabel('Datos')

%Figura del error
figure
plot(error)
title('Error')
xlabel('X');ylabel('Error')