function a = sisdifuso(x)
%Generación del sistema difuso a emplear

%Sistema difuso tipo Sugeno
a=newfis('sisdifuso','sugeno');
a.andMethod = 'prod';
a.orMethod = 'max';
a.defuzzMethod = 'wtaver';
a.impMethod = 'prod';
a.aggMethod = 'sum';

%Entrada 1
a=addvar(a,'input','entrada1',[-1.5 4]);
a=addmf(a,'input',1,'in1mf1','gbellmf',[x(1) x(2) x(3)]);
a=addmf(a,'input',1,'in1mf2','gbellmf',[x(4) x(5) x(6)]);

%Entada 2
a=addvar(a,'input','entrada2',[-1.5 4]);
a=addmf(a,'input',2,'in2mf1','gbellmf',[x(7) x(8) x(9)]);
a=addmf(a,'input',2,'in2mf2','gbellmf',[x(10) x(11) x(12)]);

%Entada 3
a=addvar(a,'input','entrada3',[-1.5 4]);
a=addmf(a,'input',3,'in3mf1','gbellmf',[x(13) x(14) x(15)]);
a=addmf(a,'input',3,'in3mf2','gbellmf',[x(16) x(17) x(18)]);

% %Salida con funciones ''linear''
% a=addvar(a,'output','salida',[-1.5 4]);
% a=addmf(a,'output',1,'out1mf1','linear',[x(13) x(14) x(15) x(16)]);
% a=addmf(a,'output',1,'out1mf2','linear',[x(17) x(18) x(19) x(20)]);
% a=addmf(a,'output',1,'out1mf3','linear',[x(21) x(22) x(23) x(24)]);
% a=addmf(a,'output',1,'out1mf4','linear',[x(25) x(26) x(27) x(28)]);
% a=addmf(a,'output',1,'out1mf5','linear',[x(29) x(30) x(31) x(32)]);
% a=addmf(a,'output',1,'out1mf6','linear',[x(33) x(34) x(35) x(36)]);
% a=addmf(a,'output',1,'out1mf7','linear',[x(37) x(38) x(39) x(40)]);
% a=addmf(a,'output',1,'out1mf8','linear',[x(41) x(42) x(43) x(44)]);

%Salida con funciones ''constan''
a=addvar(a,'output','plazo',[-1.5 4]);
a=addmf(a,'output',1,'out1mf1','constant',x(19));
a=addmf(a,'output',1,'out1mf2','constant',x(20));
a=addmf(a,'output',1,'out1mf3','constant',x(21));
a=addmf(a,'output',1,'out1mf4','constant',x(22));
a=addmf(a,'output',1,'out1mf5','constant',x(23));
a=addmf(a,'output',1,'out1mf6','constant',x(24));
a=addmf(a,'output',1,'out1mf7','constant',x(25));
a=addmf(a,'output',1,'out1mf8','constant',x(26));

%Salida con funciones ''constan''
a=addvar(a,'output','prestamo',[-1.5 4]);
a=addmf(a,'output',2,'out2mf1','constant',x(19));
a=addmf(a,'output',2,'out2mf2','constant',x(20));
a=addmf(a,'output',2,'out2mf3','constant',x(21));
a=addmf(a,'output',2,'out2mf4','constant',x(22));
a=addmf(a,'output',2,'out2mf5','constant',x(23));
a=addmf(a,'output',2,'out2mf6','constant',x(24));
a=addmf(a,'output',2,'out2mf7','constant',x(25));
a=addmf(a,'output',2,'out2mf8','constant',x(26));



%Reglas
ruleList=[   
         1   1   1   1   1   1;
         1   1   2   2   1   1; 
         1   2   1   3   1   1; 
         1   2   2   4   1   1;
         2   1   1   5   1   1;
         2   1   2   6   1   1;
         2   2   1   7   1   1;
         2   2   2   8   1   1;
         ];

a = addrule(a,ruleList);
