%Aplicaci�n de redes neuronales para la predicci�n de una se�al ECG

close all
clear all
warning off

%Primero se cargan los datos de la se�al ECG
load DatosECG

%Tomando el m�ximo y el m�nimo
minino = min(ecg_x1);
maximo = max(ecg_x1);

%Se presentan los datos
plot(ecg_x1)

%Pasando la se�al a simulink
N = length(ecg_x1);
time = 1:N;
ecgData = [time' ecg_x1'];

%Generando los respectivos retardos de la se�al en simulink
sim('SimulacionECGR16')

%Para el entrenamiento se toma 70% de datos
Xecg = XECG(1:400,:)';
Yecg = YECG(1:400,1)';
t=1:400;
plot(t,Yecg)

%Red neuronal feed forward de dos capas metodo de entrenamiento backpropagation
net = newff([minino maximo;minino maximo;minino maximo],[4 4 1],{'tansig' 'tansig' 'purelin'},'trainlm');

%Simulaci�n de la red sin entrenar
y = sim(net,Xecg);
plot(t,Yecg,t,y)
 
%Par�metros de entrenamiento
net.trainParam.epochs = 150;
net.trainParam.goal = 0.01;

%Entrenamiento de la red
net = train(net,Xecg,Yecg);

%Simulaci�n de la red entrenada
y = sim(net,Xecg);
plot(t,Yecg,t,y)
       
%Comparaci�n con todos los datos
Xecg2 = XECG';
Yecg2 = YECG';
y2 = sim(net,Xecg2);
t2 = 1:length(y2);
plot(t2,Yecg2,'b',t2,y2,'r')

%Figura del error
e = Yecg2-y2;
plot(e)
title('Grafico de error')

%Valor del MSE
%mse = (1/length(e))*sum(e.^2)
mse = maximo*0.02
maximo
