%Aplicaci�n empleando ANFIS predicci�n de series de tiempo

%Serie de tiempo Mackey-Glass 
%dx(t)/dt = 0.2x(t-tau)/(1+x(t-tau)^10) - 0.1x(t)
%Sistema ca�tico x(0) = 1.2 y tau = 17
%se considera x(t) = 0 cuando t < 0.

close all
clear all
warning off

%Cargar la serie de tiempo
load mgdata.dat

%Tomando los datos
a=mgdata;
time = a(:, 1);
ts = a(:, 2);
plot(time, ts);
xlabel('Tiempo (sec)'); ylabel('x(t)');
title('Mackey-Glass serie de tiempo ca�tica');

%Datos de entrenamiento y de comprobaci�n
%Se emplean 500 datos de entrenamiento
%La predicci�n se realiza mediante 3 muestras de entrada y 1 salida
trn_data = zeros(500, 4);
chk_data = zeros(500, 4);

%Codificaci�n de los datos de entrenamiento
START = 101;
start = START - 3;
trn_data(:, 1) = ts(start:start+500-1);
start = START - 2;
trn_data(:, 2) = ts(start:start+500-1);
start = START - 1;
trn_data(:, 3) = ts(start:start+500-1);
start = START - 0;
trn_data(:, 4) = ts(start:start+500-1);

%Codificaci�n de los datos de comprobaci�n
START = 601;
start = START - 3;
chk_data(:, 1) = ts(start:start+500-1);
start = START - 2;
chk_data(:, 2) = ts(start:start+500-1);
start = START - 1;
chk_data(:, 3) = ts(start:start+500-1);
start = START - 0;
chk_data(:, 4) = ts(start:start+500-1);

%Gr�fica de los datos de entrenamiento y de comprobaci�n
index = 118:1117+1;
figure
plot(time(index), ts(index));
axis([min(time(index)) max(time(index)) min(ts(index)) max(ts(index))]);
xlabel('Time (sec)'); ylabel('x(t)');
title('Mackey-Glass serie de tiempo ca�tica');

%Generaci�n del sistema difuso a emplear
fismat = genfis1(trn_data,[4 4 4],char('gbellmf','gbellmf','gbellmf'));

%Presentaci�n de las funciones de pertenenc�a empleadas
figure
for input_index=1:3
    subplot(2,2,input_index)
    [x,y]=plotmf(fismat,'input',input_index);
    plot(x,y)
    axis([-inf inf 0 1.2]);
    xlabel(['Input ' int2str(input_index)]);
end

%Entrenamiento del sistema difuso mediante anfis
[trn_fismat,trn_error] = anfis(trn_data, fismat,[],[],chk_data)

%Presentaci�n de las funciones de pertenencia entrenadas
figure
for input_index=1:3
    subplot(2,2,input_index)
    [x,y]=plotmf(trn_fismat,'input',input_index);
    plot(x,y)
    axis([-inf inf 0 1.2]);
    xlabel(['Input ' int2str(input_index)]);
end

%Simulaci�n del sistema con todos los datos
X = [trn_data(:,1:3);chk_data(:,1:3)];
Y = [trn_data(:,4);chk_data(:,4)];
Ys = evalfis(X,trn_fismat);

%Presentaci�n de resultados
figure
hold on
plot(Y,'r')
plot(Ys,'b')
hold off
xlabel('Tiempo (seg)'); ylabel('x(t)');
title('Mackey-Glass serie de tiempo ca�tica');
legend('Reales', 'Simulados')

%Figura del error
e = Y - Ys;
figure
plot(e)
xlabel('Tiempo (seg)'); ylabel('e(t)');
title('Error');

%Error cuadr�tico medio
N = length(e);
MSE = (1/N)*sum(e.^2)
