%Ejemplo del sistema de l�gica difusa de la propina
%implementado en comandos

close all
clear all
warning('off')

%Sistema
a=newfis('Credito Bancario');

%Variable de entrada: Cliente
a=addvar(a,'input','Cliente',[15 74]);

%Funciones de pertenencia
a=addmf(a,'input',1,'Joven','gaussmf',[5 25]);
a=addmf(a,'input',1,'Adulto','gaussmf',[5 45]);
a=addmf(a,'input',1,'Viejo','gaussmf',[5 67]);
%plotmf(a,'input',1)

%Variable de entrada: Ingresos
a=addvar(a,'input','Ingresos',[200 1000]);

%Funciones de pertenencia
a=addmf(a,'input',2,'Bajos','gaussmf',[50 400]);
a=addmf(a,'input',2,'Medios','gaussmf',[50 600]);
a=addmf(a,'input',2,'Altos','gaussmf',[50 825]);
%a=addmf(a,'input',2,'Buena','trapmf',[7 8 20 20]);
%plotmf(a,'input',2)

%Variable de entrada: Capacidad de Pago
a=addvar(a,'input','Capacidad_Pago',[20 300]);

%Funciones de pertenencia
a=addmf(a,'input',3,'Minimo','gaussmf',[20 75]);
a=addmf(a,'input',3,'Promedio','gaussmf',[20 150]);
a=addmf(a,'input',3,'Maximo','gaussmf',[20 200]);
%a=addmf(a,'input',2,'Buena','trapmf',[7 8 20 20]);
%plotmf(a,'input',3)

%Variable de salida: Plazo
a=addvar(a,'output','Plazo',[0 45]);

%Funciones de pertenencia
a=addmf(a,'output',1,'Corto','trimf',[2 12 20]);
a=addmf(a,'output',1,'Prolongado','trimf',[17 30 45]);
%a=addmf(a,'output',1,'Buena','trimf',[13 15 17]);
%plotmf(a,'output',1)

%Variable de salida: Prestamo
a=addvar(a,'output','Prestamo',[0 25]);

%Funciones de pertenencia
a=addmf(a,'output',2,'Bajo','trimf',[0 3 6]);
a=addmf(a,'output',2,'Promedio','trimf',[5 11 18]);
a=addmf(a,'output',2,'Alto','trimf',[16 19 22]);
%Los valores estan dados en numero de salarios minimos legales vigentes 

%Reglas de inferencia
ruleList=[
  	1 0 0 1 0 1 2
   	2 0 0 2 0 1 2
    3 0 0 1 0 1 2
    0 1 1 0 1 1 2
    0 2 2 0 2 1 2
    0 3 3 0 3 1 2];

a = addrule(a,ruleList);

%Sistema difuso
fuzzy(a)

%Evaluar el sistema
%Individual
%Y = evalfis([29 650 100],a)

%Para evaluar varias entradas
Datos = [29 650 100
    48 700 120
    31 800 80
    70 550 130];

%Varios
Y = evalfis(Datos,a)

