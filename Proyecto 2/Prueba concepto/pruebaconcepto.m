%Aplicación red neuronal FF multicapa
close all
clear all
warning off

%Rango de los datos de entrada
R = [0 100; 
     0 1000;
     0 300];

%Configuración capas y neuronas 
S=[20 20 19 20 1];

%Red neuronal FF
%net = newff(R,S,{'hardlim','hardlim'});
net = newff(R,S,{'tansig','tansig','tansig','tansig','purelin'});

%Datos entrada
entradas = [37 34 46 44 30 48 22 53 21 43 25 26 68 41 27 50 24;
248 350 248 480 350 600 248 500 248 800 700 248 600 450 248 600 248;
50 60 50 110 50 150 20 100 50 200 90 10 120 90 30 205 50];

%Datos y salida
salidas1 = [28.5032 19.5773 30.6623 30.6492 11.9960 30.6639 11.3191 29.9536 11.2947 30.6345 11.6528 11.3757 11.3348 30.5600 11.4190 30.6573 11.3419];
salidas2 = [3.002 3.0015 3.0002 7.5539 3.0002 11.5542 3 6.7147 3.0002 17.84 6.6524 3.001 10.8252 3.4008 3.001 13.6822 3.2];

%Simulación sin entrenar
Y = sim(net,entradas)

%Entrenamiento para plazo
net.trainParam.min_grad=0;
net = train(net,entradas,salidas1);

%Simulación con entrenamiento
net2 = newff(R,S,{'tansig','tansig','tansig','tansig','purelin'});

Y2 = sim(net2,entradas)

%entrenamiento para prestamo
net2.trainParam.min_grad=0.00001;
net2 = train(net2,entradas,salidas2);

%Y = round(sim(net,entradas))
%Y2 = round(sim(net2,entradas))

%eje x
t = [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17];

%Simulación con entrenamiento
Y = sim(net,entradas);
display(Y)
plot(t,salidas1)
hold on
plot(t,Y)
hold off
legend('plazo real','plazo simulado')
%Simulación con entrenamiento 2
Y2 = sim(net2,entradas);
plot(t,salidas2,t,Y2)
hold off
legend('Plazo','Prestamo')

%Figura del error 1
e = salidas1-Y;
display(e)
figure
plot(e)
hold on

%Figura del error 2
e2=salidas2-Y2;
figure
plot(e2)
hold off
legend('Error Plazo','Error Prestamo')
%Valor del MSE plazo
mse = (1/length(e))*sum(e.^2)

%Valor del MSE prestamo
mse2 = (1/length(e2))*sum(e2.^2)