function ft = ControlD(h)
%Dise�o del controlador con conjuntos difusos

%Sensor A
%h = (0:0.1:1)';
%A = trapmf(h, [4.5 5.5 100 100]);
A = trapmf(h, [0.9 1.15 100 100]);
%A = smf(h, [0.90 1.0]);
%plot(h, A);

%Sensor B
%h = (0:0.1:1)';
%B = trapmf(h, [0.5 1.5 100 100]);
B = trapmf(h, [0.60 0.76 100 100]);
%B = smf(h, [0.65 0.75]);
%plot(h, B);

%Sensor C
%h = (0:0.1:1)';
%M = trapmf(h, [2.5 3.5 100 100]);
C = trapmf(h, [0.35 0.51 100 100]);
%C = smf(h, [0.40 0.50]);
%plot(h, C);

%Sensor D
%h = (0:0.1:1)';
%M = trapmf(h, [2.5 3.5 100 100]);
D = trapmf(h, [0.15 0.31 100 100]);
%D = smf(h, [0.15 0.25]);
%plot(h, D);

%Funciones de activaci�n
V1=max((1-D),min(C,(1-A)));
V2=(1-C);
V3=(1-B);

%Salida total entre: 0 y 3 Lt/s
ft = 0.325*V1 + 0.42*V2 + 0.19*V3;
