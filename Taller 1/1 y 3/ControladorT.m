function Ft = ControladorT(X)
%Funci�n que implementa el controlador Booleano 

%Sensores
A = X(1);
B = X(2);
C = X(3);
D = X(4);

%Funciones de activaci�n
Y1=max((1-D),min(C,(1-A)));
Y2=(1-C);
Y3=(1-B);

%Salida total entre: 0 y 3 Lt/s
Ft = 0.325*Y1 + 0.42*Y2 + 0.19*Y3;