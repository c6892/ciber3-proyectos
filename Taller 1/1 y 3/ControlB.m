function ft = ControlB(h)
%Dise�o del controlador con conjuntos Booleanos

%Sensor A
if h < 1.0
    A = 0;
else
    A = 1;
end

%Sensor B
if h < 0.75
    B = 0;
else
    B = 1;
end

%Sensor C
if h < 0.50
    C = 0;
else
    C = 1;
end

%Sensor D
if h < 0.25
    D = 0;
else
    D = 1;
end

%Funciones de activaci�n
Y1=max((1-D),min(C,(1-A)));
Y2=(1-C);
Y3=(1-B);

%Salida total entre: 0 y 3 Lt/s
ft = 0.28*Y1 + 0.3*Y2 + 0.475*Y3;
