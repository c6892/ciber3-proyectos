function Ft = ControladorT_1(X)
%Función que implementa el controlador Booleano 

%Sensores
A= X(1);
B= X(2);
C= X(3);
D= X(4);

%Funciones de activación
Y1=max((1-D),min(C,(1-A)));
Y2=(1-C);
Y3=(1-B);

%Salida total entre: 0 y 3 Lt/s
Ft = 0.28*Y1 + 0.3*Y2 + 0.475*Y3;